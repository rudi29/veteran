//
//  HomeViewController.swift
//  veteran
//
//  Created by MacBook Pro on 05/05/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import UIKit
import WebKit

class HomeViewController: UIViewController {

    @IBOutlet weak var webViewHome: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webViewHome.load(URLRequest(url: URL(string: StaticVariables.getNewsUrl)!))
    }
    

   
}
