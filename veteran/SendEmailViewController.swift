//
//  SendEmailViewController.swift
//  veteran
//
//  Created by MacBook Pro on 05/05/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import UIKit
import MaterialComponents
import SwiftyJSON

class SendEmailViewController: UIViewController, UITextFieldDelegate {

    let localStorageData = LocalStorageData()
    let functionFactory = FunctionFactory()
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnVerify: MDCButton!
    
    var user_id:String = ""
    var email:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //** button design **//
        let shapeScheme = MDCShapeScheme()
        shapeScheme.smallComponentShape = MDCShapeCategory(cornersWith: MDCShapeCornerFamily.rounded, andSize: 20)
        
        let buttonScheme = MDCButtonScheme()
        buttonScheme.shapeScheme = shapeScheme
        
        let colorScheme : MDCColorScheming = {
            let scheme = MDCSemanticColorScheme(defaults: .material201804)
            scheme.primaryColor = UIColor.red
            return scheme
        }()
        
        MDCContainedButtonThemer.applyScheme(buttonScheme, to: self.btnVerify)
        MDCButtonColorThemer.applySemanticColorScheme(colorScheme, to: self.btnVerify)
        //** *** **//
        
        
        txtEmail.delegate = self
        txtEmail.text = email
        
        //dismiss keyboard when tap ouside the textfield
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)

    }
    
    @IBAction func btnBackOnClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnVerifyOnClick(_ sender: Any) {
        print("btnVerifyOnClick")
        if(validateInput()){
            self.sendEmailAPI(email: self.email, user_id: self.user_id)
        }
    }
    
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer){
        self.txtEmail.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func validateInput() -> Bool{
        if(self.txtEmail.text == ""){
            self.functionFactory.showSnackBar(message: StaticVariables.emptyEmail)
            return false
        }
        
        return true
    }
    
    
    //** API SECTION **//
    @objc func sendEmailAPI(email: String, user_id: String){
        if Reachability.isConnectedToNetwork() == true {
            if let url = URL(string: StaticVariables.verificationUrl){
                
//                let alert = UIAlertController(title: nil, message: "Mengirimkan Email", preferredStyle: .alert)
//
//                let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//                loadingIndicator.hidesWhenStopped = true
//                loadingIndicator.style = UIActivityIndicatorView.Style.gray
//                loadingIndicator.startAnimating()
//
//                alert.view.addSubview(loadingIndicator)
//                self.present(alert, animated: true, completion: nil)
                
                let request = NSMutableURLRequest(url:url)
                request.httpMethod = "POST"
                request.timeoutInterval = TimeInterval(120)
                
                let paramString = "user_id=" + user_id +
                "&category=1"
                
//                let paramString = "email=" + email +
//                    "&user_id=" + user_id +
//                    "&category_id=1"
                
                print("param \(paramString)")
                
                //                request.addValue("", forHTTPHeaderField: "")
                request.httpBody = paramString.data(using: String.Encoding.utf8)
                
                let task = URLSession.shared.dataTask(with:request as URLRequest){
                    data, response, error in
                    if error != nil{
                        print("error \(error)")
                        
                    }else{
                        let json = JSON(data as Any)
                        let jsonStatus = json["status"]
                        print(json)
                    }
                }
                task.resume()
            }else{
//                self.dismiss(animated: true){
                    self.functionFactory.showSnackBar(message: StaticVariables.checkYourNetworkConnection)
//                }
            }
            DispatchQueue.main.async(execute: {
                
//                self.dismiss(animated: true){
                    self.localStorageData.setIsUserLoggedIn(isUserLoggedIn: true)
                    let tabBarController = (self.storyboard?.instantiateViewController(withIdentifier: "vMenu")) as! UITabBarController
                    tabBarController.selectedIndex = 0
                    tabBarController.modalPresentationStyle = .fullScreen
                    self.functionFactory.getTopMostViewController()?.present(tabBarController, animated: true)
//                }
            })
        }
    }
    //** *** **//

}
