//
//  FunctionFactory.swift
//  veteran
//
//  Created by MacBook Pro on 05/05/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import Foundation
import MaterialComponents.MaterialSnackbar

class FunctionFactory{
    func showSnackBar(message: String){
        let msg = MDCSnackbarMessage()
        msg.text = message
        MDCSnackbarManager.show(msg)
    }
    
    func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
        
        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }
        
        return topMostViewController
    }
}

