//
//  StaticVariables.swift
//  veteran
//
//  Created by MacBook Pro on 05/05/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import Foundation
import UIKit

struct StaticVariables{
    static let getNewsUrl = "https://pothan.kemhan.go.id/category/veteran-ri"
    static let registrationUrl = "http://103.214.53.140/kemhan_api/auth/register"
    static let loginUrl = "http://103.214.53.140/kemhan_api/auth/login"
    static let sendEmailUrl = "http://103.214.54.126:8080/rest/api/send_email"
    static let dashboardUrl = "https://veteran.kemhan.go.id/chart"
    static let pendaftaranVeteranUrl = "https://veteran.kemhan.go.id/signup"
    static let verificationUrl = "http://103.214.53.140/kemhan_api/auth/verification"

    
    //Validation Input Message
    static let emptyEmail = "Email tidak boleh kosong"
    static let emptyPassword = "Password tidak boleh kosong"
    static let emptyPasswordConfirmation = "Konfirmasi Password tidak boleh kosong"
    
    //
    static let checkYourNetworkConnection = "Periksa koneksi internet anda"

}
