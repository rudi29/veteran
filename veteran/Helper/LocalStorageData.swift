//
//  LocalStorageData.swift
//  veteran
//
//  Created by MacBook Pro on 05/05/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import Foundation

class LocalStorageData{
    //** SETTER **//
    func setIsUserLoggedIn(isUserLoggedIn: Bool){
        UserDefaults.standard.set(isUserLoggedIn, forKey: "isUserLoggedIn")
    }
    //** *** **//
    
    
    
    //** GETTER ** //
    func getIsUserLoggedIn() -> Bool{
        return Bool(UserDefaults.standard.bool(forKey: "isUserLoggedIn"))
    }
    //** *** **//
    
}
