//
//  LoginPortalUIView.swift
//  veteran
//
//  Created by MacBook Pro on 05/05/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import UIKit
import MaterialComponents


class LoginPortalUIView: UIView {

    @IBOutlet weak var btnLogin: MDCButton!
    @IBOutlet weak var btnRegistrasi: MDCButton!
    @IBOutlet weak var btnPendaftaranVeteran: MDCButton!
    
    override func layoutSubviews() {
        let shapeScheme = MDCShapeScheme()
            shapeScheme.smallComponentShape = MDCShapeCategory(cornersWith: MDCShapeCornerFamily.rounded, andSize: 20)
        
        let buttonScheme = MDCButtonScheme()
            buttonScheme.shapeScheme = shapeScheme
        
        let colorScheme : MDCColorScheming = {
            let scheme = MDCSemanticColorScheme(defaults: .material201804)
            scheme.primaryColor = UIColor.red
            return scheme
        }()
        
        /** Login button **/
        MDCContainedButtonThemer.applyScheme(buttonScheme, to: self.btnLogin)
        MDCButtonColorThemer.applySemanticColorScheme(colorScheme, to: self.btnLogin)
        /** *** **/
        
        /** Registration button **/
        MDCOutlinedButtonThemer.applyScheme(buttonScheme, to: self.btnRegistrasi)
        self.btnRegistrasi.setBorderColor(UIColor.red, for: .normal)
        self.btnRegistrasi.setTitleColor(UIColor.black, for: .normal)
        /** ** **/
        
        /** Pendaftaran Veteran button **/
        MDCOutlinedButtonThemer.applyScheme(buttonScheme, to: self.btnPendaftaranVeteran)
        self.btnPendaftaranVeteran.setBorderColor(UIColor.red, for: .normal)
        self.btnPendaftaranVeteran.setTitleColor(UIColor.black, for: .normal)
        /** ** **/
    }
}
