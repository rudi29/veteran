//
//  RegisterViewController.swift
//  veteran
//
//  Created by MacBook Pro on 05/05/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import UIKit
import MaterialComponents
import SwiftyJSON

class RegisterViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var btnRegistrasi: MDCButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtKonfirmasiPassword: UITextField!
    @IBOutlet weak var btnShowHidePassword: UIButton!
    @IBOutlet weak var btnShowHideKonfirmasiPassword: UIButton!
    
    let functionFactory = FunctionFactory()
    var clickPasswordHint = true
    var clickKonfirmasiPasswordHint = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtPassword.isSecureTextEntry = true
        self.txtKonfirmasiPassword.isSecureTextEntry = true
        
        //** button design **//
        let shapeScheme = MDCShapeScheme()
        shapeScheme.smallComponentShape = MDCShapeCategory(cornersWith: MDCShapeCornerFamily.rounded, andSize: 20)
        
        let buttonScheme = MDCButtonScheme()
        buttonScheme.shapeScheme = shapeScheme
        
        let colorScheme : MDCColorScheming = {
            let scheme = MDCSemanticColorScheme(defaults: .material201804)
            scheme.primaryColor = UIColor.red
            return scheme
        }()
        
        MDCContainedButtonThemer.applyScheme(buttonScheme, to: self.btnRegistrasi)
        MDCButtonColorThemer.applySemanticColorScheme(colorScheme, to: self.btnRegistrasi)
        //** *** **//
        
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtKonfirmasiPassword.delegate = self
        
        //dismiss keyboard when tap ouside the textfield
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @IBAction func btnBackOnClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnRegistrasiOnClick(_ sender: Any) {
        print("btnRegistrasiOnCLick")
        if(validateInput()){
            self.registerAPI(email: self.txtEmail.text!, password: self.txtPassword.text!)
        }
    }
    
    @IBAction func btnShowHidePasswordOnClick(_ sender: Any) {
        if(clickPasswordHint == true) {
            self.txtPassword.isSecureTextEntry = false
            self.btnShowHidePassword.setBackgroundImage(UIImage.init(named: "ic_show_password"), for: .normal)
        } else {
            self.txtPassword.isSecureTextEntry = true
            self.btnShowHidePassword.setBackgroundImage(UIImage.init(named: "ic_hide_password"), for: .normal)
        }
        
        clickPasswordHint = !clickPasswordHint
    }
    
    @IBAction func btnShowHideKonfirmasiPasswordOnClick(_ sender: Any) {
        if(clickKonfirmasiPasswordHint == true) {
            self.txtKonfirmasiPassword.isSecureTextEntry = false
            self.btnShowHideKonfirmasiPassword.setBackgroundImage(UIImage.init(named: "ic_show_password"), for: .normal)
        } else {
            self.txtKonfirmasiPassword.isSecureTextEntry = true
            self.btnShowHideKonfirmasiPassword.setBackgroundImage(UIImage.init(named: "ic_hide_password"), for: .normal)
        }
        
        clickKonfirmasiPasswordHint = !clickKonfirmasiPasswordHint
    }
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer){
        self.txtEmail.resignFirstResponder()
        self.txtPassword.resignFirstResponder()
        self.txtKonfirmasiPassword.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func validateInput() -> Bool{
        if(self.txtEmail.text == ""){
            self.functionFactory.showSnackBar(message: StaticVariables.emptyEmail)
            return false
        }
        
        if(self.txtPassword.text == ""){
            self.functionFactory.showSnackBar(message: StaticVariables.emptyPassword)
            return false
        }
        
        if(self.txtKonfirmasiPassword.text == ""){
            self.functionFactory.showSnackBar(message: StaticVariables.emptyPasswordConfirmation)
            return false
        }
        
        return true
    }
    
    
    //** API SECTION **//
    @objc func registerAPI(email: String, password: String){
        if Reachability.isConnectedToNetwork() == true {
            if let url = URL(string: StaticVariables.registrationUrl){
                
                let alert = UIAlertController(title: nil, message: "Registrasi", preferredStyle: .alert)
                
                let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                loadingIndicator.hidesWhenStopped = true
                loadingIndicator.style = UIActivityIndicatorView.Style.gray
                loadingIndicator.startAnimating()
                
                alert.view.addSubview(loadingIndicator)
                self.present(alert, animated: true, completion: nil)
                
                let request = NSMutableURLRequest(url:url)
                request.httpMethod = "POST"
                request.timeoutInterval = TimeInterval(120)
                
                
                let paramString = "username=" + email +
                    "&password=" + password +
                    "&category=1"
                
                
                print("param \(paramString)")
                
                request.httpBody = paramString.data(using: String.Encoding.utf8)
                
                let task = URLSession.shared.dataTask(with:request as URLRequest){
                    data, response, error in
                    if error != nil{
                        DispatchQueue.main.async(execute: {
                            self.dismiss(animated: true){
                                self.functionFactory.showSnackBar(message: StaticVariables.checkYourNetworkConnection)
                            }
                        })
                    }else{
                        do {
                            DispatchQueue.main.async(execute: {
                                self.dismiss(animated: true){
                                    let json = JSON(data as Any)
                                    let jsonStatus = json["status"]
                                    
                                    if jsonStatus == "success" {
                                        let jsonData = JSON(json["data"] as Any)
                                        let user_id = jsonData["user_id"]
                                        let username = jsonData["username"]
                                        let fullname = jsonData["fullname"]
                                        let password = jsonData["password"]
                                        let user_email = jsonData["email"]
                                        let access_token = jsonData["access_token"]
                                        let category = jsonData["category_id"]
                                        let approved = jsonData["approved"]

                                        //Printing result//
                                        print("***json Data***")
                                        print("user_id \(user_id)")
                                        print("username \(username)")
                                        print("fullname \(fullname)")
                                        print("password \(password)")
                                        print("user_email \(user_email)")
                                        print("access_token \(access_token)")
                                        print("category \(category)")
                                        print("approved \(approved)")
                                        print("***")
                                        //
                                        
                                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "vSendEmail") as? SendEmailViewController
                                        nextViewController!.email = self.txtEmail.text!
                                        nextViewController!.user_id = "\(user_id)"
                                        self.functionFactory.getTopMostViewController()?.present(nextViewController!, animated: true, completion: nil)
                                        
                                    }else if jsonStatus == "error" {
                                        let message = json["message"]
                                        self.functionFactory.showSnackBar(message: "\(message)")
                                    }
                                }
                            })
                            
                            
                        }
                    }
                }
                task.resume()
            }else{
                DispatchQueue.main.async(execute: {
                    self.dismiss(animated: true){
                        self.functionFactory.showSnackBar(message: StaticVariables.checkYourNetworkConnection)
                    }
                })
            }
        }
    }
    //** *** **//
}
