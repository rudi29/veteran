//
//  LoginViewController.swift
//  veteran
//
//  Created by MacBook Pro on 05/05/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import UIKit
import MaterialComponents
import SwiftyJSON

class LoginViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var btnLogin: MDCButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnShowHidePassword: UIButton!
    
    let localStorageData = LocalStorageData()
    let functionFactory = FunctionFactory()
    var clickPasswordHint = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtPassword.isSecureTextEntry = true
        
        //** button design **//
        let shapeScheme = MDCShapeScheme()
        shapeScheme.smallComponentShape = MDCShapeCategory(cornersWith: MDCShapeCornerFamily.rounded, andSize: 20)
        
        let buttonScheme = MDCButtonScheme()
        buttonScheme.shapeScheme = shapeScheme
        
        let colorScheme : MDCColorScheming = {
            let scheme = MDCSemanticColorScheme(defaults: .material201804)
            scheme.primaryColor = UIColor.red
            return scheme
        }()
        
        MDCContainedButtonThemer.applyScheme(buttonScheme, to: self.btnLogin)
        MDCButtonColorThemer.applySemanticColorScheme(colorScheme, to: self.btnLogin)
        //** *** **//
        
        txtEmail.delegate = self
        txtPassword.delegate = self
        
        //dismiss keyboard when tap ouside the textfield
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @IBAction func btnLoginOnClick(_ sender: Any) {
        //redirect back to home
        print("btnLoginOnClick")
        if(validateInput()){
            self.loginAPI(email: self.txtEmail.text!, password: self.txtPassword.text!)
        }
        
       
    }
    
    @IBAction func btnShowHidePasswordOnClick(_ sender: Any) {
        if(clickPasswordHint == true) {
            self.txtPassword.isSecureTextEntry = false
            self.btnShowHidePassword.setBackgroundImage(UIImage.init(named: "ic_show_password"), for: .normal)
        } else {
            self.txtPassword.isSecureTextEntry = true
            self.btnShowHidePassword.setBackgroundImage(UIImage.init(named: "ic_hide_password"), for: .normal)
        }
        
        clickPasswordHint = !clickPasswordHint
    }
    
    @IBAction func btnBackOnClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer){
        self.txtEmail.resignFirstResponder()
        self.txtPassword.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func validateInput() -> Bool{
        if(self.txtEmail.text == ""){
            self.functionFactory.showSnackBar(message: StaticVariables.emptyEmail)
            return false
        }
        
        if(self.txtPassword.text == ""){
            self.functionFactory.showSnackBar(message: StaticVariables.emptyPassword)
            return false
        }
        
        return true
    }
    
    
    //** API SECTION **//
    @objc func loginAPI(email: String, password: String){
        if Reachability.isConnectedToNetwork() == true {
            if let url = URL(string: StaticVariables.loginUrl){

                
                let alert = UIAlertController(title: nil, message: "Login", preferredStyle: .alert)

                let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                loadingIndicator.hidesWhenStopped = true
                loadingIndicator.style = UIActivityIndicatorView.Style.gray
                loadingIndicator.startAnimating()

                alert.view.addSubview(loadingIndicator)
                self.present(alert, animated: true, completion: nil)
                
                let request = NSMutableURLRequest(url:url)
                request.httpMethod = "POST"
                request.timeoutInterval = TimeInterval(120)
                

                let paramString = "username=" + email +
                    "&password=" + password +
                    "&category=1"
                
                print("param \(paramString)")
                
//                request.addValue("", forHTTPHeaderField: "")
                request.httpBody = paramString.data(using: String.Encoding.utf8)
                
                let task = URLSession.shared.dataTask(with:request as URLRequest){
                    data, response, error in
                    if error != nil{
                       
                        self.dismiss(animated: true){
                            self.functionFactory.showSnackBar(message: StaticVariables.checkYourNetworkConnection)
                        }
                    }else{
                        do {
                            DispatchQueue.main.async(execute: {
                                
                                self.dismiss(animated: true){
                                    let json = JSON(data as Any)
                                    let jsonStatus = json["status"]
                                    print(json)
                                    print(jsonStatus)
                                    if jsonStatus == "success" {
                                        let jsonData = JSON(json["data"] as Any)
                                        print(jsonData)
                                        let email = jsonData["email"]
                                        let user_id = jsonData["user_id"]
                                        let approved = jsonData["approved"]
                                        let category = jsonData["category_id"]
                                        let access_token = jsonData["access_token"]
                                        let username = jsonData["username"]
                                        
                                        //Printing result//
                                        print("***json Data***")
                                        print("access_token \(access_token)")
                                        print("user_id \(user_id)")
                                        print("email \(email)")
                                        print("username \(username)")
                                        print("category \(category)")
                                        print("approved \(approved)")
                                        print("category_id \(category)")
                                        print("***")
                                        //
                                        
                                        self.localStorageData.setIsUserLoggedIn(isUserLoggedIn: true)
                                        
                                        let tabBarController = (self.storyboard?.instantiateViewController(withIdentifier: "vMenu")) as! UITabBarController
                                        tabBarController.selectedIndex = 0
                                        tabBarController.modalPresentationStyle = .fullScreen
                                        self.functionFactory.getTopMostViewController()?.present(tabBarController, animated: false)
                                        
                                    }else if jsonStatus == "error" {
                                        let message = json["message"]
                                        self.functionFactory.showSnackBar(message: "\(message)")
                                    }
                                }
                            })
                            

                        }
                    }
                }
                task.resume()
            }else{
                self.dismiss(animated: true){
                
                    self.functionFactory.showSnackBar(message: StaticVariables.checkYourNetworkConnection)
                }
            }
        }
    }
    //** *** **//
}
