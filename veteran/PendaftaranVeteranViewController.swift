//
//  PendaftaranVeteranViewController.swift
//  veteran
//
//  Created by MacBook Pro on 06/05/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import UIKit
import WebKit
class PendaftaranVeteranViewController: UIViewController {

    @IBOutlet weak var webKitPendaftaran: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webKitPendaftaran.load(URLRequest(url: URL(string: StaticVariables.pendaftaranVeteranUrl)!))
        
    }
    
    @IBAction func btnBackOnClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
