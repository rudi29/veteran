//
//  ProfileViewController.swift
//  veteran
//
//  Created by MacBook Pro on 05/05/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import UIKit
import WebKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var btnLogout: UIBarButtonItem!
    @IBOutlet weak var loginPortalUIView : LoginPortalUIView!
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var webProfileUIView: WKWebView!
    //    @IBOutlet weak var navigationBar: UINavigationBar!
    
    var rightBarButtonItem : UIBarButtonItem!
    
    let localStorageData = LocalStorageData()
    let functionFactory = FunctionFactory()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        if(!self.localStorageData.getIsUserLoggedIn()){
            webProfileUIView.isHidden = true
            containerUIView.isHidden = false
            
            self.btnLogout.isEnabled = false
            self.btnLogout.title = ""
            
            self.showLoginPortal()
        }else{
            containerUIView.isHidden = true
            webProfileUIView.isHidden = false
            
            self.btnLogout.isEnabled = true
            self.btnLogout.title = "Logout"
            
            
            self.showWebView()
        }
    }
    
    private func showLoginPortal(){
        //userIsLoggedIn = false
        print("showLoginPortal")
        let nibLoginPortalUIView = UINib(nibName: "LoginPortalUIView", bundle: nil)
        self.loginPortalUIView = (nibLoginPortalUIView.instantiate(withOwner: self, options: nil).first as! LoginPortalUIView)
        self.loginPortalUIView.frame = CGRect(x: self.containerUIView.frame.minX, y: 0, width: self.containerUIView.frame.width, height: self.containerUIView.frame.height)
       
        self.containerUIView.addSubview(self.loginPortalUIView)
        
        self.loginPortalUIView.btnLogin.addTarget(self, action: #selector(btnLoginOnClick(_:)), for: .touchUpInside)
        self.loginPortalUIView.btnRegistrasi.addTarget(self, action: #selector(btnRegisterOnClick(_:)), for: .touchUpInside)
        self.loginPortalUIView.btnPendaftaranVeteran.addTarget(self, action: #selector(btnPendaftaranVeteranOnClick(_:)), for: .touchUpInside)
        
    }
    
    private func showWebView(){
        //userIsLoggedIn = true
        print("showWebView")
        self.webProfileUIView.load(URLRequest(url: URL(string: StaticVariables.dashboardUrl)!))
        
    }
    
    @IBAction func btnLoginOnClick(_ sender: UIButton){
        print("btnLoginOnClick")
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "vLogin") as? LoginViewController
        nextViewController!.modalPresentationStyle = .fullScreen
        self.functionFactory.getTopMostViewController()?.present(nextViewController!, animated: true, completion: nil)
    }
    
    @IBAction func btnRegisterOnClick(_ sender: UIButton){
        print("btnRegisterOnClick")
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "vRegister") as? RegisterViewController
        nextViewController!.modalPresentationStyle = .fullScreen
        self.functionFactory.getTopMostViewController()?.present(nextViewController!, animated: true, completion: nil)
        
    }
    
    @IBAction func btnPendaftaranVeteranOnClick(_ sender: UIButton){
        print("btnPendaftaranVeteranOnClick")
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "vPendaftaranVeteran") as? PendaftaranVeteranViewController
        nextViewController!.modalPresentationStyle = .fullScreen
        self.functionFactory.getTopMostViewController()?.present(nextViewController!, animated: true, completion: nil)
        
    }
    
    @IBAction func btnLogoutOnClick(_ sender: Any) {
        print("btnLogoutOnClick")
        self.localStorageData.setIsUserLoggedIn(isUserLoggedIn: false)
        print("isUserLogin \(self.localStorageData.getIsUserLoggedIn())")
        
        let tabBarController = (self.storyboard?.instantiateViewController(withIdentifier: "vMenu")) as! UITabBarController
        tabBarController.selectedIndex = 0
        tabBarController.modalPresentationStyle = .fullScreen
        self.functionFactory.getTopMostViewController()?.present(tabBarController, animated: false)
        
        
    }
    
}
